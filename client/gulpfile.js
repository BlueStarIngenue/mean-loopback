require('babel-core/register');

var gulp = require('gulp');
var browserSync = require('browser-sync');  
var proxy = require('proxy-middleware');  
var url = require('url');
var Karma = require('karma').Server;

gulp.task('serve', function(done) {

  var proxyOptionsAccessControl = function(req,res, next){
        res.setHeader('Access-Control-Allow-Origin', '*');
        next();
  };
  var proxyOptionsApiRoute = url.parse('http://localhost:3000/api') ;
  proxyOptionsApiRoute.route = '/api';

  var proxyOptionsAuthRoute = url.parse('http://localhost:3000/auth') ;
  proxyOptionsAuthRoute.route = '/auth';

  browserSync({
    open: false,
    port: 4000,
    server: {
      baseDir: ['.'],
      middleware: [
        proxyOptionsAccessControl, 
        proxy(proxyOptionsApiRoute), 
        proxy(proxyOptionsAuthRoute)]
    }
  }, done);
});

gulp.task('test', function (done) {
    new Karma({
        configFile: __dirname +  '/karma.conf.js',
        singleRun: true
    }, function (errorCode) {
        var error = new Error("Tests have failed.");
        error.showStack = false;

        done(errorCode ? error : 0)
    })
    .start();
});