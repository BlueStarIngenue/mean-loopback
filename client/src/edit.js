﻿import {inject} from 'aurelia-framework';
import {PlantService} from './plantService';
import {Router} from 'aurelia-router';
import {Validation} from 'aurelia-validation';
import {ensure} from 'aurelia-validation';

@inject(PlantService, Router, Validation)
export class EditPlant {
    
    heading = 'Edit Plant';

    @ensure(function(it){ it.isNotEmpty() })
    imageUrl = '';

    @ensure(function(it){ it.isNotEmpty().hasLengthBetween(1,100) })
    commonName = '';

    @ensure(function(it){ it.isNotEmpty().hasLengthBetween(1,100) })
    family = '';

    @ensure(function(it){ it.isNotEmpty().hasLengthBetween(1,100) })
    genus = '';

    constructor(plantService, router, validation) {
        this.plantService = plantService;
        this.router = router;
        this.validation = validation.on(this);
    }

    activate(params) {
        let plant = this.plantService.getPlant(params.id).then((plant) => {
            this.imageUrl = plant.imageUrl;
            this.commonName = plant.commonName;
            this.family = plant.family;
            this.genus = plant.genus;
            this.id = plant.id;
        });
    }

    update() {
        let newPlant = {
            imageUrl: this.imageUrl,
            commonName: this.commonName,
            family: this.family,
            genus: this.genus,
            id: this.id
        };
        this.validation.validate().then( () => {
            this.plantService.update(newPlant);
            this.router.navigate('plants');
        }).catch(error => {
        });
    }
}