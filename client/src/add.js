﻿import {inject} from 'aurelia-framework';
import {PlantService} from './plantService';
import {Router} from 'aurelia-router';
import {ensure, Validation} from 'aurelia-validation';
import {UserService} from './userService';

@inject(PlantService, Router, Validation, UserService)
export class AddPlant {
    
    heading = 'Add Plant';

    @ensure(function(it){ it.isNotEmpty() })
    imageUrl = '';

    @ensure(function(it){ it.isNotEmpty().hasLengthBetween(1,100) })
    commonName = '';

    @ensure(function(it){ it.isNotEmpty().hasLengthBetween(1,100) })
    family = '';

    @ensure(function(it){ it.isNotEmpty().hasLengthBetween(1,100) })
    genus = '';

    constructor(plantService, router, validation, userService) {
        this.plantService = plantService;
        this.router = router;
        this.validation = validation.on(this);
        this.userService = userService;
    }

    activate() {
        if (!this.userService.isAuthenticated()) {
            this.router.navigate('login')
        }
    }

    add() {
        let newPlant = {
            imageUrl: this.imageUrl,
            commonName: this.commonName,
            family: this.family,
            genus: this.genus
        };
        return this.validation.validate().then(() => {
            this.plantService.add(newPlant).then(() => {
                this.router.navigate('plants');
            });
             }).catch(error => {
        });
    }
}