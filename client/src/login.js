﻿import {inject} from 'aurelia-framework';
import {UserService} from './userService';
import {Router} from 'aurelia-router';
import {ensure, Validation} from 'aurelia-validation';

@inject(UserService, Router, Validation)
export class Login {
    
    heading = 'Log In';

    @ensure(function(it){ it.isNotEmpty() })
    email = '';

    @ensure(function(it){ it.isNotEmpty() })
    password = '';

    constructor(userService, router, validation) {
        this.userService = userService;
        this.router = router;
        this.validation = validation.on(this);
    }

    login() {
        return this.validation.validate().then(() => {
            this.userService.login(this.email,this.password).then(() => {
                this.router.navigate('plants');
            });
        }).catch(error => {
        });
    }
}