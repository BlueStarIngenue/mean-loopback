﻿import {bindable} from 'aurelia-framework';
import {inject} from 'aurelia-framework';
import {UserService} from './userService';

@inject(UserService)

export class NavBar {
    @bindable router = null;

    constructor(auth) {
        this.auth = auth;
    };

    // We can check if the user is authenticated
    // to conditionally hide or show nav bar items
    get isAuthenticated() {
        return this.auth.isAuthenticated();
    };
}