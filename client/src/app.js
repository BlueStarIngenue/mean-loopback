﻿export class App {
    configureRouter(config, router) {
        config.title = 'Plants Plants & Plants';
        config.map([
          { route: ['','plants'], name: 'plants', moduleId: './plants', nav: true, title:'Plants' },
          { route: 'edit/:id', name: 'edit', moduleId: './edit', nav: false, title:'Edit Plant' },
          { route: 'add', name: 'add', moduleId: './add', nav: true, title:'Add Plant' },
          { route: 'login', name: 'login', moduleId: './login', nav: false, title:'Log In'},
          { route: 'logout', name: 'logout', moduleId: './logout', nav: false, title:'Log Out'}
        ]);

        this.router = router;
    };
}