﻿import {inject} from 'aurelia-framework';
import {PlantService} from './plantService';
import {Router} from 'aurelia-router';

@inject(PlantService, Router)
export class Plants {
    heading = 'Plants & More Plants';
    plants = [];

    constructor(plantService, router) {
        this.plantService = plantService;
        this.router = router;
    }

    activate() {
        this.loadPlants();
    }

    loadPlants() {
        this.plantService.getPlants().then((plants) => {
            this.plants = plants;
        }).catch(()=> {
            this.router.navigate('login')
        });
    }

    delete(id) {
    if (confirm("Delete this plant?") == true) {
        this.plantService.delete(id).then(() => {
            this.loadPlants();
        });
    } else {
        activate();
    }
    }
}