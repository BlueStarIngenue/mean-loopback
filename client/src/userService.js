﻿import {HttpClient} from 'aurelia-fetch-client';
import {inject} from 'aurelia-framework';

@inject(HttpClient)
export class UserService {

    accessToken = null;

    constructor(http) {
        this.http = http;
    }

    login(email,password) {
        return this.http.fetch('login',{
            method: 'post',
            body: JSON.stringify({
                email:email,
                password:password
            })
        }).then((response) => {
            return response.json();
        }).then((data)=> {
            if (data.accessToken) {
                this.accessToken = data.accessToken;
            } else {
                throw new Error("Failed to log in");
            }
        });
    }

    logout() {
        return this.http.fetch('logout',{
            method: 'post',
            headers: {
                Authorization: this.accessToken
            }
        }).then((response) => {
            this.accessToken = null;
        });
    }

    getAccessToken() {
        return this.accessToken;
    }

    isAuthenticated() {
        return this.accessToken != null;
    }
}