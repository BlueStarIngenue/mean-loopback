﻿import {inject} from 'aurelia-framework';
import {UserService} from './userService';
import {Router} from 'aurelia-router';

@inject(UserService, Router)
export class Logout {

    constructor(userService, router) {
        this.userService = userService;
        this.router = router;
    }

    activate() {
        this.userService.logout().then(() => {
            this.router.navigate('login');
        }).catch(error => {
        });
    }
}