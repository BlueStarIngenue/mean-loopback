﻿import { TWBootstrapViewStrategy } from 'aurelia-validation';
import {HttpClient} from 'aurelia-fetch-client';

export function configure(aurelia) {
    aurelia.use
      .standardConfiguration()
      .developmentLogging()
      .plugin('aurelia-validation', (config) => { config.useViewStrategy(TWBootstrapViewStrategy.AppendToInput) });

    configureContainer(aurelia.container);

    aurelia.start().then(a => a.setRoot('app', document.body));
}

function configureContainer(container) {
    let http = new HttpClient();
    http.configure(config => {
        config
          .useStandardConfiguration()
          .withBaseUrl('http://localhost:4000/api/')
          .withDefaults({
            headers: {
                'content-type': 'application/json',
                'Accept': 'application/json',
                'X-Requested-With': 'Fetch'
            }
        })
    });
    container.registerInstance(HttpClient, http);
}