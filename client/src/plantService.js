﻿import {HttpClient} from 'aurelia-fetch-client';
import {inject} from 'aurelia-framework';
import {UserService} from './userService';

@inject(HttpClient, UserService)
export class PlantService {

    constructor(http, userService) {
        this.http = http;
        this.userService = userService;
    }


    getPlants() {
        return this.http.fetch('plants',{
            headers: {
                Authorization: this.userService.getAccessToken()
            }
        }).then((response) => {
            return response.json();
        });
    }

    getPlant(id) {
        return this.http.fetch(`plants/${id}`,{
            headers: {
                Authorization: this.userService.getAccessToken()
            }
        }).then((response) => {
            return response.json();
        });
    }

    update(newPlant) {
        return this.http.fetch(`plants/${newPlant.id}`,{
            method: 'put',
            body: JSON.stringify(newPlant),
            headers: {
            Authorization: this.userService.getAccessToken()
    }
        });
    }

    add(newPlant) {
        return this.http.fetch('plants',{
            method: 'post',
            body: JSON.stringify(newPlant),
            headers: {
                Authorization: this.userService.getAccessToken()
            }
        });

    }

    delete(id) {
        return this.http.fetch(`plants/${id}`,{
            method: 'delete'
        });
    }
}