﻿import {AddPlant} from "../src/add";


describe("AddPlant", function() {

    let plantService;
    let router;
    let validation;

    beforeEach(function() {
        plantService = jasmine.createSpyObj("plantSerivce", ["add"]);
        router = jasmine.createSpyObj("router", ["navigate"]);
        validation = jasmine.createSpyObj("validation", ["on", "validate"]);
        validation.on.and.returnValue(validation);
    });

    it("adds new plant through plantService when add() is triggered", function(done) {
        validation.validate.and.returnValue(Promise.resolve());
        let component = new AddPlant(plantService, router, validation);
       
        component.imageUrl = "test image URL";
        component.commonName = "test common name";
        component.family = "test family";
        component.genus = "test genus";

        return component.add().then(() => {
            expect(plantService.add).toHaveBeenCalledWith({
                imageUrl: "test image URL",
                commonName: "test common name",
                family: "test family",
                genus: "test genus"
            });
            done();
        });
    });

    it("does not call plantService when validation fails", function(done) {
        validation.validate.and.returnValue(Promise.reject());
        let component = new AddPlant(plantService, router, validation);

        return component.add().then(() => {
            expect(plantService.add).not.toHaveBeenCalled();
            done();
        });

    });
});

