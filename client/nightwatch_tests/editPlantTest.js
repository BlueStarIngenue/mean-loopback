﻿module.exports = {
    'Edit Plant Happy Path': function (browser) {
        browser
          .url('http://localhost:4000')
          .waitForElementVisible('a[href="#/edit/3"]', 100000)
          .click('a[href="#/edit/3"]')
          .waitForElementVisible('button[type=submit]', 100000)

          .clearValue('input[name=imageUrl]')
          .setValue('input[name=imageUrl]', 'NightWatch Selenium Test Image URL')

          .clearValue('input[name=commonName]')
          .setValue('input[name=commonName]', 'NightWatch Selenium Test Common Name')

          .clearValue('input[name=family]')
          .setValue('input[name=family]', 'NightWatch Selenium Test Family')

          .clearValue('input[name=genus]')
          .setValue('input[name=genus]', 'NightWatch Selenium Test Genus')

          .click('button[type=submit]')
          .waitForElementVisible('.card-container', 100000)

          .assert.urlEquals('http://localhost:4000/#/plants')
          .assert.containsText('.card-container:last-of-type p[class=family]', 'NightWatch Selenium Test Family')

          .end();
    },

    'Edit Plant Sad Path': function (browser) {
        browser
          .url('http://localhost:4000')
          .waitForElementVisible('a[href="#/edit/3"]', 100000)
          .click('a[href="#/edit/3"]')
          .waitForElementVisible('button[type=submit]', 100000)

          .clearValue('input[name=imageUrl]')
          .clearValue('input[name=commonName]')
          .clearValue('input[name=family]')
          .clearValue('input[name=genus]')

          .click('button[type=submit]')
          .assert.urlEquals('http://localhost:4000/#/edit/3')
          .assert.containsText("input[name=imageUrl] + p", 'required')

          .end();
    }

};