﻿module.exports = {
    'Add Plant Happy Path': function (browser) {
        browser
          .url('http://localhost:4000')
          .waitForElementVisible('a[href="#/add"]', 100000)
          .click('a[href="#/add"]')
          .waitForElementVisible('button[type=submit]', 100000)

          .setValue('input[name=imageUrl]', 'NightWatch Selenium Test Image URL')
          .setValue('input[name=commonName]', 'NightWatch Selenium Test Common Name')
          .setValue('input[name=family]', 'NightWatch Selenium Test Family')
          .setValue('input[name=genus]', 'NightWatch Selenium Test Genus')

          .click('button[type=submit]')
          .waitForElementVisible('.card-container', 100000)

          .assert.urlEquals('http://localhost:4000/#/plants')
          .assert.containsText('.card-container:last-of-type p[class=family]', 'NightWatch Selenium Test Family')

          .end();
    },

        'Add Plant Sad Path': function (browser) {
            browser
              .url('http://localhost:4000')
              .waitForElementVisible('a[href="#/add"]', 100000)
              .click('a[href="#/add"]')
              .waitForElementVisible('button[type=submit]', 100000)

              .click('button[type=submit]')

              .assert.urlEquals('http://localhost:4000/#/add')
              .assert.containsText("input[name=imageUrl] + p", 'required')

              .end();
        }

};