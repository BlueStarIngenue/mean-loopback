﻿module.exports = function (app) {
    app.dataSources.memory.automigrate('plant', function (err) {
        if (err) throw err;

        app.models.plant.create([
        {
            imageUrl: 'https://upload.wikimedia.org/wikipedia/commons/5/54/Lithops_salicola.jpg',
            commonName: 'Living Stone',
            family: 'Aizoaceae',
            genus: 'Lithops',
            id: 1,
        },
        {
            imageUrl: 'https://upload.wikimedia.org/wikipedia/commons/d/d9/Honeysuckle-1.jpg',
            commonName: 'Honeysuckle',
            family: 'Caprifoliaceae',
            genus: 'Lonicera',
            id: 2
        },
        {
            imageUrl: 'https://upload.wikimedia.org/wikipedia/commons/e/ed/Sacred_lotus_Nelumbo_nucifera.jpg',
            commonName: 'Lotus',
            family: 'Nelumbonaceae',
            genus: 'Nelumbo',
            id: 3
        }
        ], function (err) {
            if (err) throw err;
        });
    });
};