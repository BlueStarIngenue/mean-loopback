﻿module.exports = function (app) {
    var router = app.loopback.Router();

    router.post('/api/login', function (req, res) {
        var email = req.body.email;
        var password = req.body.password;

        app.models.user.login({
            email: email,
            password: password
        }, 'admin', function (err, token) {
            if (err) {
                return res.send({
                    loginFailed: true
                });
            }

            token = token.toJSON();

            res.send({
                user: token.userId,
                accessToken: token.id
            });
        });
    });

    router.post('/api/logout', function (req, res) {
        var accessTokenId = req.accessToken;

        app.models.user.logout(accessTokenId, function (err) {
            return res.send({
                loggedOut: true
            });
        });
    });

    app.use(router);
};